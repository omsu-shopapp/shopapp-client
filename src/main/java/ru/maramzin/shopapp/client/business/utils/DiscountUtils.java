package ru.maramzin.shopapp.client.business.utils;

public class DiscountUtils {

  public static Integer discountedPrice(Integer price, Integer discount) {
    return price - ((price * discount) / 100);
  }
}
