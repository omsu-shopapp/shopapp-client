package ru.maramzin.shopapp.client.business.dto.response.success;

import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.OrderResponse;

@Data
@Builder
public class CancelOrderResponse {

  private OrderResponse orderInfo;
}
