package ru.maramzin.shopapp.client.business.domain;

import java.util.UUID;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.dto.request.RegisterClientRequest;
import ru.maramzin.shopapp.client.business.entity.Address;
import ru.maramzin.shopapp.client.business.entity.Cart;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.StringUtils;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Getter
@Builder
public class RegisterClient extends BaseDomain<RegisterClient> {

  @Getter(AccessLevel.NONE)
  private RegisterClientRequest request;
  private Client client;
  private Address address;

  @Override
  public RegisterClient validate() {
    try {
      check(StringUtils.isBlank(request.getEmail()), "Email is not provided.");
      check(StringUtils.isBlank(request.getPhoneNumber()), "Phone number is not provided.");
      check(StringUtils.isBlank(request.getFirstname()), "Firstname is not provided.");
      check(StringUtils.isBlank(request.getSurname()), "Surname is not provided.");
    } catch (ValidationException exception) {
      errorMessage.append(exception.getMessage());
    }

    return this;
  }

  @Override
  public RegisterClient execute() {
    if(!isFailed()) {
      address = Address.builder()
          .id(UUID.randomUUID())
          .city(request.getAddress().getCity())
          .district(request.getAddress().getDistrict())
          .street(request.getAddress().getStreet())
          .house(request.getAddress().getHouse())
          .apartment(request.getAddress().getApartment())
          .zipcode(request.getAddress().getZipcode())
          .build();

      client = Client.builder()
          .id(UUID.randomUUID())
          .email(request.getEmail())
          .phoneNumber(request.getPhoneNumber())
          .firstname(request.getFirstname())
          .surname(request.getSurname())
          .cart(Cart.builder().id(UUID.randomUUID()).build())
          .address(address)
          .build();
    }

    return this;
  }

  @Override
  public RegisterClient ifSuccess(Action<RegisterClient> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }
    return this;
  }

  @Override
  public RegisterClient rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }
}
