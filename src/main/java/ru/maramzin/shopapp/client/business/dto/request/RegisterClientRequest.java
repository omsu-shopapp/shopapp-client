package ru.maramzin.shopapp.client.business.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegisterClientRequest {

  private String email;
  private String phoneNumber;
  private String firstname;
  private String surname;
  private RegisterAddressRequest address;
}
