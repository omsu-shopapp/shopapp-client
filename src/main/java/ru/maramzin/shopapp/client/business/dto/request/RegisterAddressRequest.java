package ru.maramzin.shopapp.client.business.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegisterAddressRequest {

  private String city;
  private String district;
  private String street;
  private String house;
  private Integer apartment;
  private String zipcode;
}
