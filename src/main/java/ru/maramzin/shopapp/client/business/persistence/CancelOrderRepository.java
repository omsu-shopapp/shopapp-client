package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.domain.CancelOrder;
import ru.maramzin.shopapp.client.business.entity.Order;

public interface CancelOrderRepository extends Storage<CancelOrder>, Searcher<Order> {
}
