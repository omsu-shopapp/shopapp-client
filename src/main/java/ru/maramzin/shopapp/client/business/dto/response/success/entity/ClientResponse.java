package ru.maramzin.shopapp.client.business.dto.response.success.entity;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientResponse {

  private UUID id;
  private String email;
  private String phoneNumber;
  private String firstname;
  private String surname;
  private Integer balance;
  private AddressResponse address;
  private CartResponse cart;
  private List<OrderResponse> orders;
}
