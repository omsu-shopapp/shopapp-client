package ru.maramzin.shopapp.client.business.entity.enumeration;

public enum DeliveryStatus {
  PREPARATION, IN_WAY, DELIVERED, CANCELLED, EXPIRED
}
