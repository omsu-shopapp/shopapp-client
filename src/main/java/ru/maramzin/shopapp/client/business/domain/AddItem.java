package ru.maramzin.shopapp.client.business.domain;

import java.util.Objects;
import java.util.UUID;
import lombok.Builder;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.dto.request.entity.ItemRequest;
import ru.maramzin.shopapp.client.business.entity.Cart;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.entity.Item;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Builder
public class AddItem extends BaseDomain<AddItem> {

  ItemRequest itemRequest;

  private Client client;

  private Item item;

  @Override
  public AddItem validate() {
    try {
      check(Objects.isNull(client), "Client is not found.");
    } catch (ValidationException e) {
      errorMessage.append(e.getMessage());
    }

    return this;
  }

  @Override
  public AddItem execute() {
    if(!isFailed()) {
      item = Item.builder()
          .id(UUID.randomUUID())
          .productName(itemRequest.getProductName())
          .quantity(itemRequest.getQuantity())
          .cart(client.getCart())
          .build();
    }

    return this;
  }

  @Override
  public AddItem ifSuccess(Action<AddItem> action){
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public AddItem rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }

  public Item getItem() {
    return item;
  }

  public Cart getCart() {
    return client.getCart();
  }
}
