package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.OrderConverter;
import ru.maramzin.shopapp.client.business.domain.CancelOrder;
import ru.maramzin.shopapp.client.business.dto.request.CancelOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.CancelOrderResponse;
import ru.maramzin.shopapp.client.business.persistence.CancelOrderRepository;

@Service
@RequiredArgsConstructor
public class CancelOrderConverter implements
    DomainConverter<CancelOrderRequest, CancelOrder, CancelOrderResponse> {

  private final CancelOrderRepository repository;
  private final OrderConverter orderConverter;

  @Override
  public CancelOrder convertToDomain(CancelOrderRequest request) {
    return CancelOrder.builder()
        .order(repository.findById(request.getOrderId()).orElse(null))
        .build();
  }

  @Override
  public CancelOrderResponse convertToResponse(CancelOrder domain) {
    return CancelOrderResponse.builder()
        .orderInfo(orderConverter.toResponse(domain.getOrder()))
        .build();
  }
}
