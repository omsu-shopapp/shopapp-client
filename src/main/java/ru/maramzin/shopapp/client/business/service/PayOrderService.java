package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.adapter.PayOrderStorageAdapter;
import ru.maramzin.shopapp.client.business.converter.domain.PayOrderConverter;
import ru.maramzin.shopapp.client.business.domain.PayOrder;
import ru.maramzin.shopapp.client.business.dto.request.PayOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.PayOrderResponse;
import ru.maramzin.shopapp.client.business.persistence.PayOrderRepository;

@Service
@RequiredArgsConstructor
public class PayOrderService implements RequestProcessor<PayOrderRequest, PayOrderResponse> {

  private final PayOrderRepository repository;
  private final PayOrderConverter converter;
  private final PayOrderStorageAdapter adapter;

  @Override
  @Transactional
  public Result<PayOrderResponse, RequestFailed> process(PayOrderRequest request) {
    PayOrder domain = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(adapter::commitPurchase)
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(domain);
  }
}
