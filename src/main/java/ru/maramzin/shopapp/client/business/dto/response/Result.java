package ru.maramzin.shopapp.client.business.dto.response;

public interface Result<S, F> {

  S getSuccess();
  F getFailed();

  default boolean isSuccess() {
    return getSuccess() != null;
  }
}
