package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.converter.domain.FindClientConverter;
import ru.maramzin.shopapp.client.business.domain.FindClient;
import ru.maramzin.shopapp.client.business.dto.request.FindClientRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.FindClientResponse;

@Service
@RequiredArgsConstructor
public class FindClientService implements RequestProcessor<FindClientRequest, FindClientResponse> {

  private final FindClientConverter converter;

  @Override
  @Transactional
  public Result<FindClientResponse, RequestFailed> process(FindClientRequest request) {
    FindClient domain = converter.convertToDomain(request)
        .validate()
        .execute()
        .rollbackIfFailed();

    return converter.convertToResult(domain);
  }
}
