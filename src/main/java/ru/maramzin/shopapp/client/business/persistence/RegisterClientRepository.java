package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.domain.RegisterClient;

public interface RegisterClientRepository extends Storage<RegisterClient> {
}
