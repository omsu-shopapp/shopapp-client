package ru.maramzin.shopapp.client.business.dto.ingress;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.ingress.enumeration.ReservationStatus;

@Data
@Builder
public class ReservationResponse {

  private UUID id;
  private UUID clientId;
  private ReservationStatus status;
  private List<ReservedProductResponse> reservedProducts;
}
