package ru.maramzin.shopapp.client.business.converter.entity;

public interface EntityConverter<E, RS> {

  RS toResponse(E entity);
}
