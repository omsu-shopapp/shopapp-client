package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.entity.Client;

public interface FindClientRepository extends Searcher<Client> {
}
