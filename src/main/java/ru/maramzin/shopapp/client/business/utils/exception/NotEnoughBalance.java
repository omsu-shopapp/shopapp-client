package ru.maramzin.shopapp.client.business.utils.exception;

public class NotEnoughBalance extends RuntimeException {

  public NotEnoughBalance(String message) {
    super(message);
  }
}
