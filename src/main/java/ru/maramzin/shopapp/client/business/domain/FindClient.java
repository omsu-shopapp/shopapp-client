package ru.maramzin.shopapp.client.business.domain;

import java.util.Objects;
import lombok.Builder;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Builder
public class FindClient extends BaseDomain<FindClient> {

  Client client;

  @Override
  public FindClient validate() {
    try {
      check(Objects.isNull(client), "Client not found. ");
    } catch (ValidationException e) {
      errorMessage.append(e.getMessage());
    }

    return this;
  }

  @Override
  public FindClient execute() {
    return this;
  }

  @Override
  public FindClient ifSuccess(Action<FindClient> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public FindClient rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }

  public Client getClient() {
    return client;
  }
}
