package ru.maramzin.shopapp.client.business.dto.response.success.entity;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryPlace;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus;
import ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState;

@Data
@Builder
public class OrderResponse {

  private UUID id;
  private LocalDateTime creationTime;
  private DeliveryStatus deliveryStatus;
  private PaymentState paymentState;
  private DeliveryPlace deliveryPlace;
  private CartResponse cart;
  private AddressResponse address;
  private UUID clientId;
  private UUID reservationId;
}
