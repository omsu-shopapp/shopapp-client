package ru.maramzin.shopapp.client.business.dto.response.success;

import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.CartResponse;

@Data
@Builder
public class AddItemResponse {

  private CartResponse cart;
}
