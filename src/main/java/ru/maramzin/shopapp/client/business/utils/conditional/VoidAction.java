package ru.maramzin.shopapp.client.business.utils.conditional;

@FunctionalInterface
public interface VoidAction {

  void perform();
}
