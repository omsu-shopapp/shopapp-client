package ru.maramzin.shopapp.client.business.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryPlace;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus;
import ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "orders")
@Entity
public class Order {

  @Id
  private UUID id;

  private LocalDateTime creationTime;

  private LocalDateTime closingTime;

  @Enumerated(EnumType.STRING)
  private DeliveryStatus deliveryStatus;

  @Enumerated(EnumType.STRING)
  private PaymentState paymentState;

  @Enumerated(EnumType.STRING)
  private DeliveryPlace deliveryPlace;

  private UUID reservationId;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "cart_id")
  private Cart cart;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "address_id")
  private Address address;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "client_id")
  private Client client;
}