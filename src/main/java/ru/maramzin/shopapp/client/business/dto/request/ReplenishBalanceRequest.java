package ru.maramzin.shopapp.client.business.dto.request;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReplenishBalanceRequest {

  private UUID clientId;
  private Integer amount;
}
