package ru.maramzin.shopapp.client.business.dto.request.entity;

import lombok.Data;

@Data
public class ItemRequest {

  private String productName;
  private Integer quantity;
}
