package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.converter.domain.AddItemConverter;
import ru.maramzin.shopapp.client.business.domain.AddItem;
import ru.maramzin.shopapp.client.business.dto.request.AddItemRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.AddItemResponse;
import ru.maramzin.shopapp.client.business.persistence.AddItemRepository;

@Service
@RequiredArgsConstructor
public class AddItemService implements RequestProcessor<AddItemRequest, AddItemResponse> {

  private final AddItemRepository repository;
  private final AddItemConverter converter;

  @Override
  @Transactional
  public Result<AddItemResponse, RequestFailed> process(AddItemRequest request) {
    AddItem domain = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(domain);
  }
}
