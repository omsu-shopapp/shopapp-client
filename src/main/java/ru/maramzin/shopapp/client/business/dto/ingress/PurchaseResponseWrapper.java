package ru.maramzin.shopapp.client.business.dto.ingress;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PurchaseResponseWrapper {

  private List<PurchaseResponse> content;
  private UUID reservationId;
}
