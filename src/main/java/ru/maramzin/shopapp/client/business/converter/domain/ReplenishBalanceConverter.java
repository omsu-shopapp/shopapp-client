package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.ClientConverter;
import ru.maramzin.shopapp.client.business.domain.ReplenishBalance;
import ru.maramzin.shopapp.client.business.dto.request.ReplenishBalanceRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.ReplenishBalanceResponse;
import ru.maramzin.shopapp.client.business.persistence.ReplenishBalanceRepository;

@Service
@RequiredArgsConstructor
public class ReplenishBalanceConverter implements DomainConverter<ReplenishBalanceRequest,
    ReplenishBalance, ReplenishBalanceResponse> {

  private final ReplenishBalanceRepository repository;
  private final ClientConverter clientConverter;

  @Override
  public ReplenishBalance convertToDomain(ReplenishBalanceRequest request) {
    return ReplenishBalance.builder()
        .amount(request.getAmount())
        .client(repository.findById(request.getClientId()).orElse(null))
        .build();
  }

  @Override
  public ReplenishBalanceResponse convertToResponse(ReplenishBalance domain) {
    return ReplenishBalanceResponse.builder()
        .clientInfo(clientConverter.toResponse(domain.getClient()))
        .build();
  }
}
