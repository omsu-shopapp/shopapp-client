package ru.maramzin.shopapp.client.business.adapter;

import ru.maramzin.shopapp.client.business.domain.CreateOrder;
import ru.maramzin.shopapp.client.business.utils.exception.NotEnoughBalance;

public interface CreateOrderStorageAdapter {

  void  purchase(CreateOrder domain) throws NotEnoughBalance;
}
