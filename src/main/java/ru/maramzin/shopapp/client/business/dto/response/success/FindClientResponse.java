package ru.maramzin.shopapp.client.business.dto.response.success;

import lombok.Builder;
import lombok.Data;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.ClientResponse;

@Data
@Builder
public class FindClientResponse {

  private ClientResponse clientInfo;
}
