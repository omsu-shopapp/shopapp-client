package ru.maramzin.shopapp.client.business.dto.egress;

import lombok.Builder;

@Builder
public record PurchaseRequest(String productName, Integer quantity) {
}
