package ru.maramzin.shopapp.client.business.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maramzin.shopapp.client.business.adapter.CancelOrderStorageAdapter;
import ru.maramzin.shopapp.client.business.converter.domain.CancelOrderConverter;
import ru.maramzin.shopapp.client.business.domain.CancelOrder;
import ru.maramzin.shopapp.client.business.dto.request.CancelOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.Result;
import ru.maramzin.shopapp.client.business.dto.response.failed.RequestFailed;
import ru.maramzin.shopapp.client.business.dto.response.success.CancelOrderResponse;
import ru.maramzin.shopapp.client.business.persistence.CancelOrderRepository;

@Service
@RequiredArgsConstructor
public class CancelOrderService implements
    RequestProcessor<CancelOrderRequest, CancelOrderResponse> {

  private final CancelOrderRepository repository;
  private final CancelOrderConverter converter;
  private final CancelOrderStorageAdapter adapter;

  @Override
  @Transactional
  public Result<CancelOrderResponse, RequestFailed> process(CancelOrderRequest request) {
    CancelOrder domain = converter.convertToDomain(request)
        .validate()
        .execute()
        .ifSuccess(adapter::rollbackPurchase)
        .ifSuccess(repository::persist)
        .rollbackIfFailed();

    return converter.convertToResult(domain);
  }
}
