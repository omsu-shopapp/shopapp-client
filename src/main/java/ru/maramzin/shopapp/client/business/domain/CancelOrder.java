package ru.maramzin.shopapp.client.business.domain;

import java.util.Objects;
import lombok.Builder;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus;
import ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Builder
public class CancelOrder extends BaseDomain<CancelOrder> {

  private Order order;

  @Override
  public CancelOrder validate() {
    try {
      check(Objects.isNull(order), "Order does not exist.");
      check(PaymentState.PAID.equals(order.getPaymentState()), "Order is already paid.");
    } catch (ValidationException exception) {
      errorMessage.append(exception.getMessage());
    }

    return this;
  }

  @Override
  public CancelOrder execute() {
    if(!isFailed()) {
      order.setDeliveryStatus(DeliveryStatus.CANCELLED);
    }

    return this;
  }

  @Override
  public CancelOrder ifSuccess(Action<CancelOrder> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public CancelOrder rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }

  public Order getOrder() {
    return order;
  }
}
