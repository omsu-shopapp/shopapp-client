package ru.maramzin.shopapp.client.business.entity.enumeration;

public enum PaymentState {

  PAID, NOT_PAID
}
