package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.domain.ReplenishBalance;
import ru.maramzin.shopapp.client.business.entity.Client;

public interface ReplenishBalanceRepository extends Storage<ReplenishBalance>, Searcher<Client> {

}
