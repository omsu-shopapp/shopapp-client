package ru.maramzin.shopapp.client.business.converter.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maramzin.shopapp.client.business.converter.entity.OrderConverter;
import ru.maramzin.shopapp.client.business.domain.CreateOrder;
import ru.maramzin.shopapp.client.business.dto.request.CreateOrderRequest;
import ru.maramzin.shopapp.client.business.dto.response.success.CreateOrderResponse;
import ru.maramzin.shopapp.client.business.persistence.CreateOrderRepository;

@Service
@RequiredArgsConstructor
public class CreateOrderConverter implements DomainConverter<CreateOrderRequest, CreateOrder, CreateOrderResponse> {

  private final CreateOrderRepository repository;
  private final OrderConverter orderConverter;

  @Override
  public CreateOrder convertToDomain(CreateOrderRequest request) {
    return CreateOrder.builder()
        .client(repository.findClientById(request.getClientId()).orElse(null))
        .deliveryPlace(request.getDeliveryPlace())
        .isAlreadyPaid(request.getIsAlreadyPaid())
        .build();
  }

  @Override
  public CreateOrderResponse convertToResponse(CreateOrder domain) {
    return CreateOrderResponse.builder()
        .orderInfo(orderConverter.toResponse(domain.getOrder()))
        .build();
  }
}
