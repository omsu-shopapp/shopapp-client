package ru.maramzin.shopapp.client.business.converter.entity;

import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.ItemResponse;
import ru.maramzin.shopapp.client.business.entity.Item;

@Component
public class ItemConverter implements EntityConverter<Item, ItemResponse> {

  @Override
  public ItemResponse toResponse(Item entity) {
    return ItemResponse.builder()
        .productName(entity.getProductName())
        .quantity(entity.getQuantity())
        .build();
  }
}
