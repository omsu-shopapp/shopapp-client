package ru.maramzin.shopapp.client.business.dto.response.failed;

import lombok.Builder;

@Builder
public record RequestFailed(String errorMessage) {
}
