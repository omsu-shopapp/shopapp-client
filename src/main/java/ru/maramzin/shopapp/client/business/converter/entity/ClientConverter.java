package ru.maramzin.shopapp.client.business.converter.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.dto.response.success.entity.ClientResponse;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.entity.Order;

@Component
@RequiredArgsConstructor
public class ClientConverter implements EntityConverter<Client, ClientResponse> {

  private final CartConverter cartConverter;
  private final AddressConverter addressConverter;
  private final OrderConverter orderConverter;

  @Override
  public ClientResponse toResponse(Client entity) {
    List<Order> orders = Optional.ofNullable(entity.getOrders()).orElse(new ArrayList<>());
    return ClientResponse.builder()
        .id(entity.getId())
        .email(entity.getEmail())
        .phoneNumber(entity.getPhoneNumber())
        .firstname(entity.getFirstname())
        .surname(entity.getSurname())
        .balance(entity.getBalance())
        .cart(cartConverter.toResponse(entity.getCart()))
        .address(addressConverter.toResponse(entity.getAddress()))
        .orders(orders.stream()
            .map(orderConverter::toResponse)
            .toList())
        .build();
  }
}
