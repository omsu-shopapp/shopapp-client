package ru.maramzin.shopapp.client.business.persistence;

import ru.maramzin.shopapp.client.business.domain.PayOrder;
import ru.maramzin.shopapp.client.business.entity.Order;

public interface PayOrderRepository extends Storage<PayOrder>, Searcher<Order> {
}
