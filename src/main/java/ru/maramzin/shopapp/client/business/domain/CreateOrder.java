package ru.maramzin.shopapp.client.business.domain;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import lombok.Builder;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.entity.Cart;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryPlace;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus;
import ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Builder
public class CreateOrder extends BaseDomain<CreateOrder> {

  private Client client;
  private DeliveryPlace deliveryPlace;
  private Boolean isAlreadyPaid;
  private Order order;

  public CreateOrder validate() {
    try {
      check(Objects.isNull(client), "Client does not exist.");
      check(Objects.nonNull(client) &&
          client.getCart().getItems().isEmpty(), "Cart is empty.");
    } catch (ValidationException e) {
      errorMessage.append(e.getMessage());
    }

    return this;
  }

  @Override
  public CreateOrder execute() {
    if(!isFailed()) {
      order = Order.builder()
          .id(UUID.randomUUID())
          .creationTime(LocalDateTime.now())
          .deliveryStatus(DeliveryStatus.PREPARATION)
          .paymentState(isAlreadyPaid ? PaymentState.PAID : PaymentState.NOT_PAID)
          .deliveryPlace(Objects.isNull(deliveryPlace) ? DeliveryPlace.HOME : deliveryPlace)
          .client(client)
          .address(client.getAddress())
          .cart(client.getCart())
          .build();

      client.setCart(Cart.builder().id(UUID.randomUUID()).build());
    }

    return this;
  }

  @Override
  public CreateOrder ifSuccess(Action<CreateOrder> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public CreateOrder rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }

  public Order getOrder() {
    return order;
  }

  public Client getClient() {
    return client;
  }
}
