package ru.maramzin.shopapp.client.business.utils;

@FunctionalInterface
public interface Action<T> {

  void perform(T t);
}
