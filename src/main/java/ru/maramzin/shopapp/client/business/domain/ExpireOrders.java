package ru.maramzin.shopapp.client.business.domain;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus;
import ru.maramzin.shopapp.client.business.utils.Action;

@Builder
@Getter
public class ExpireOrders extends BaseDomain<ExpireOrders> {

  private List<Order> orders;

  @Override
  public ExpireOrders validate() {
    return this;
  }

  @Override
  public ExpireOrders execute() {
    orders.forEach(order -> order.setDeliveryStatus(DeliveryStatus.EXPIRED));
    return this;
  }

  @Override
  public ExpireOrders ifSuccess(Action<ExpireOrders> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }

    return this;
  }

  @Override
  public ExpireOrders rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }
}
