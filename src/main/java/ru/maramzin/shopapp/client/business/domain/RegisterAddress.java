package ru.maramzin.shopapp.client.business.domain;

import java.util.Objects;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import ru.maramzin.shopapp.client.business.dto.request.RegisterAddressRequest;
import ru.maramzin.shopapp.client.business.entity.Address;
import ru.maramzin.shopapp.client.business.utils.Action;
import ru.maramzin.shopapp.client.business.utils.StringUtils;
import ru.maramzin.shopapp.client.business.utils.exception.ValidationException;

@Getter
@Builder
public class RegisterAddress extends BaseDomain<RegisterAddress> {

  @Getter(AccessLevel.NONE)
  private RegisterAddressRequest request;
  private Address address;

  public RegisterAddress validate() {
    try {
      check(StringUtils.isBlank(request.getCity()), "City is not provided.");
      check(StringUtils.isBlank(request.getDistrict()), "District is not provided.");
      check(StringUtils.isBlank(request.getStreet()), "Street is not provided.");
      check(StringUtils.isBlank(request.getHouse()), "House is not provided.");
      check(Objects.isNull(request.getApartment()), "Apartment is not provided.");
      check(StringUtils.isBlank(request.getZipcode()), "Zipcode is not provided.");
    } catch (ValidationException e) {
      errorMessage.append(e.getMessage());
    }

    return this;
  }

  @Override
  public RegisterAddress execute() {
    if(!isFailed()) {
      address = Address.builder()
          .id(UUID.randomUUID())
          .city(request.getCity())
          .district(request.getDistrict())
          .street(request.getStreet())
          .house(request.getHouse())
          .apartment(request.getApartment())
          .zipcode(request.getZipcode())
          .build();
    }

    return this;
  }

  @Override
  public RegisterAddress ifSuccess(Action<RegisterAddress> action) {
    if(!isFailed()) {
      try {
        action.perform(this);
      } catch (Exception e) {
        errorMessage.append(e.getMessage());
      }
    }
    return this;
  }

  @Override
  public RegisterAddress rollbackIfFailed() {
    if(isFailed()) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
    }

    return this;
  }
}
