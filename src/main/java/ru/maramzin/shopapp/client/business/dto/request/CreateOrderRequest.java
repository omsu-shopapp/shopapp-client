package ru.maramzin.shopapp.client.business.dto.request;

import java.util.UUID;
import lombok.Data;
import ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryPlace;

@Data
public class CreateOrderRequest {
  private UUID clientId;
  private Boolean isAlreadyPaid;
  private DeliveryPlace deliveryPlace;
}
