package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.ReplenishBalance;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.persistence.ReplenishBalanceRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.ClientRepository;

@Component
@RequiredArgsConstructor
public class ReplenishBalanceRepositoryImpl implements ReplenishBalanceRepository {

  private final ClientRepository clientRepository;

  @Override
  public Optional<Client> findById(UUID id) {
    return clientRepository.findById(id);
  }

  @Override
  public void persist(ReplenishBalance domain) {
    clientRepository.save(domain.getClient());
  }
}
