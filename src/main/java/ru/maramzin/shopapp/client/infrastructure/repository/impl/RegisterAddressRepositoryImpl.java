package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.RegisterAddress;
import ru.maramzin.shopapp.client.business.persistence.RegisterAddressRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.AddressRepository;

@Component
@RequiredArgsConstructor
public class RegisterAddressRepositoryImpl implements RegisterAddressRepository {

  private final AddressRepository addressRepository;

  @Override
  public void persist(RegisterAddress domain) {
    addressRepository.save(domain.getAddress());
  }
}
