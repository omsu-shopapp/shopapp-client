package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.RegisterClient;
import ru.maramzin.shopapp.client.business.persistence.RegisterClientRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.AddressRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.ClientRepository;

@Component
@RequiredArgsConstructor
public class RegisterClientRepositoryImpl implements RegisterClientRepository {

  private final ClientRepository clientRepository;
  private final AddressRepository addressRepository;

  @Override
  public void persist(RegisterClient domain) {
    addressRepository.save(domain.getAddress());
    clientRepository.save(domain.getClient());
  }
}
