package ru.maramzin.shopapp.client.infrastructure.repository.impl;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.domain.AddItem;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.persistence.AddItemRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.ClientRepository;
import ru.maramzin.shopapp.client.infrastructure.repository.jpa.ItemRepository;

@Component
@RequiredArgsConstructor
public class AddItemRepositoryImpl implements AddItemRepository {

  private final ClientRepository clientRepository;
  private final ItemRepository itemRepository;

  @Override
  public Optional<Client> findClientById(UUID clientId) {
    return clientRepository.findById(clientId);
  }

  @Override
  public void persist(AddItem domain) {
    itemRepository.save(domain.getItem());
  }
}
