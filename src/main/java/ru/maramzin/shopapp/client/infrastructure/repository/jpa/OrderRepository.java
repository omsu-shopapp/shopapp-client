package ru.maramzin.shopapp.client.infrastructure.repository.jpa;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.maramzin.shopapp.client.business.entity.Order;

public interface OrderRepository extends JpaRepository<Order, UUID> {

  @Query("select o from Order o "
      + "where o.deliveryStatus <> ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus.DELIVERED "
      + "and o.deliveryStatus <> ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus.CANCELLED "
      + "and o.deliveryStatus <> ru.maramzin.shopapp.client.business.entity.enumeration.DeliveryStatus.EXPIRED "
      + "and o.paymentState = ru.maramzin.shopapp.client.business.entity.enumeration.PaymentState.NOT_PAID "
      + "and o.creationTime <= :max")
  List<Order> findOrdersToExpire(@Param("max") LocalDateTime maxCreationDateTime);
}