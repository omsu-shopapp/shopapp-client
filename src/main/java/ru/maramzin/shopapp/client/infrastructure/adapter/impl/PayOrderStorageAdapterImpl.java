package ru.maramzin.shopapp.client.infrastructure.adapter.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.maramzin.shopapp.client.business.adapter.PayOrderStorageAdapter;
import ru.maramzin.shopapp.client.business.domain.PayOrder;
import ru.maramzin.shopapp.client.business.dto.ingress.ProductInfoResponse;
import ru.maramzin.shopapp.client.business.dto.ingress.ReservationResponse;
import ru.maramzin.shopapp.client.business.dto.ingress.ReservedProductResponse;
import ru.maramzin.shopapp.client.business.entity.Client;
import ru.maramzin.shopapp.client.business.entity.Order;
import ru.maramzin.shopapp.client.business.utils.DiscountUtils;
import ru.maramzin.shopapp.client.business.utils.conditional.Conditional;
import ru.maramzin.shopapp.client.business.utils.exception.NotEnoughBalance;
import ru.maramzin.shopapp.client.infrastructure.adapter.client.StorageClient;

@Component
@RequiredArgsConstructor
public class PayOrderStorageAdapterImpl implements PayOrderStorageAdapter {

  private final StorageClient storageClient;

  @Override
  public void commitPurchase(PayOrder domain) {
    Order order = domain.getOrder();
    Client client = order.getClient();
    ReservationResponse reservation = storageClient.findById(order.getReservationId());
    int totalCost = reservation.getReservedProducts().stream()
        .mapToInt(this::getFinalCost)
        .sum();

    Conditional.of(totalCost <= client.getBalance())
        .then(() -> {
          storageClient.commitPurchase(order.getReservationId());
          client.setBalance(client.getBalance() - totalCost);
        })
        .orElseThrow(new NotEnoughBalance("Not enough money on balance: " + client.getBalance() +
            ", required: " + totalCost + "."));
  }

  private Integer getFinalCost(ReservedProductResponse reservedProduct) {
    ProductInfoResponse product = reservedProduct.productInfo();
    return DiscountUtils.discountedPrice(product.cost(), product.discount()) *
        reservedProduct.quantity();
  }
}
